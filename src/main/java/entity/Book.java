package entity;

/**
 * Book.
 */
public class Book {
    private int id;
    private String title;
    private Author author;
    private String description;
    private int price;

    public Book(int id, String title, Author author, String description, int price) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.description = description;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Author getAuthor() {
        return author;
    }

    public String getDescription() {
        return description;
    }

    public int getPrice() {
        return price;
    }
}