package wrappers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.regex.Pattern;

/**
 * XSS protection logic.
 */
public class XSSRequestWrapper  extends HttpServletRequestWrapper{
    private final static String REGULAR_EXPRESSION = "<script>(.*?)</script>";         // any character sequence between script tags
    private final static String STRING_FOR_REPLACEMENT = "";

    public XSSRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    /**
     * Check all parameter values.
     * @param parameter Parameter received from client.
     * @return wrapped parameters.
     */
    @Override
    public String[] getParameterValues(String parameter) {
        String[] values = super.getParameterValues(parameter);
        if (values == null) {
            return null;
        }
        int count = values.length;
        String[] encodedValues = new String[count];
        for (int i = 0; i < count; i++) {
            encodedValues[i] = stripXSS(values[i]);
        }
        return encodedValues;
    }

    /**
     * Check parameter value.
     * @param parameter Parameter received from client.
     * @return wrapped parameter.
     */
    @Override
    public String getParameter(String parameter) {
        String value = super.getParameter(parameter);
        return stripXSS(value);
    }

    /**
     * Avoid anything between script tags.
     * @param value The initial value.
     * @return value after XSS protection logic.
     */
    private String stripXSS(String value){
        Pattern scriptPattern = Pattern.compile(REGULAR_EXPRESSION, Pattern.CASE_INSENSITIVE);
        value = scriptPattern.matcher(value).replaceAll(STRING_FOR_REPLACEMENT);
        return value;
    }
}