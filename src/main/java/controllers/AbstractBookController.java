package controllers;

import controllers.constants.Attributes;
import dao.BookDao;
import dao.BookDaoFactory;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

/**
 * General functionality of all servlets.
 */
public abstract class AbstractBookController extends HttpServlet{
    BookDao bookDao;

    /**
     * Get BookDao from ServletContext.
     */
    @Override
    public void init() throws ServletException {
        bookDao = BookDaoFactory.getBookDao(getServletContext().getInitParameter(Attributes.BOOK_DAO));
    }
}