package controllers;

import controllers.constants.Attributes;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Update book author in storage.
 */
public class UpdateBookAuthorController extends AbstractBookController{

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int bookId = Integer.parseInt(request.getParameter(Attributes.BOOK_ID));
        String newBookAuthorName = request.getParameter(Attributes.NEW_BOOK_AUTHOR);
        bookDao.updateBookAuthor(newBookAuthorName, bookId);        // update book author in storage
        response.sendRedirect("/books");
    }
}