package controllers;

import controllers.constants.Attributes;
import entity.Book;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Show all books from database.
 */
public class AllBooksController extends AbstractBookController{

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Book> bookList = bookDao.getAllBooks();    // get all books from storage
        request.setAttribute(Attributes.BOOKS, bookList);
        request.getRequestDispatcher("jsp/books.jsp").forward(request, response);
    }
}