package controllers;

import controllers.constants.Attributes;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Delete book from storage (this servlet is injured).
 */
public class DeleteBookController extends AbstractBookController{
    private boolean isServletUnavailable;
    private int unavailableSeconds;
    private static final String UNAVAILABILITY = "unavailability";
    private static final String UNAVAILABLE_SECONDS = "unavailableSeconds";

    /**
     * Check unavailability.
     */
    @Override
    public void init() throws ServletException {
        super.init();
        isServletUnavailable = Boolean.parseBoolean(getServletConfig().getInitParameter(UNAVAILABILITY));   // true or false
        unavailableSeconds = Integer.parseInt(getServletConfig().getInitParameter(UNAVAILABLE_SECONDS));
    }

    /**
     * Throw an exception if servlet is injured :)
     */
    @Override
    public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
        if (isServletUnavailable){
            isServletUnavailable = false;
            throw new UnavailableException("Servlet is unavailable", unavailableSeconds);
        }
        super.service(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String bookTitle = request.getParameter(Attributes.BOOK_TITLE);
        bookDao.deleteBook(bookTitle);      // delete book from storage
        response.sendRedirect("/books");
    }
}