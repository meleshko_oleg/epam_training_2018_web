package controllers.asynchrony;

import listeners.AsyncContextListener;
import org.apache.log4j.Logger;
import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This servlet works asynchronously.
 */
public class AsyncServlet extends HttpServlet{
    private static final Logger LOGGER = Logger.getLogger(AsyncServlet.class);
    private static final String SQUARE = "square";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int numberForSquaring = Integer.parseInt(request.getParameter(SQUARE));
        int squareOfTheNumber = getSquare(numberForSquaring);
        request.setAttribute(SQUARE, squareOfTheNumber);

        // let's start to work asynchronously
        final AsyncContext asyncContext = request.startAsync();
        asyncContext.addListener(new AsyncContextListener());               // add AsyncListener
        asyncContext.start(() -> {
         try {
                Thread.sleep(3000);                                   // 3 seconds of sleeping
                asyncContext.dispatch("/cube");
            } catch (InterruptedException e) {
                LOGGER.error(e.getMessage(), e);
            }
        });
    }

    /**
     * Find the square of a number.
     * @param numberForSquaring This number will be squaring.
     * @return the square of the number.
     */
    private int getSquare(int numberForSquaring){
        return numberForSquaring * numberForSquaring;           // i don't want to use class Math :)
    }
}