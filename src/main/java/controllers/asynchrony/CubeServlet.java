package controllers.asynchrony;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This servlet gets control from an asynchronous servlet.
 */
public class CubeServlet extends HttpServlet{
    private static final String CUBE = "cube";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int numberForCubing = Integer.parseInt(request.getParameter(CUBE));
        int cubeOfTheNumber = getCube(numberForCubing);
        request.setAttribute(CUBE, cubeOfTheNumber);
        request.getRequestDispatcher("/jsp/asyncResult.jsp").forward(request, response);
    }

    /**
     * Find the getCube of a number.
     * @param numberForCubing This number will be cubing.
     * @return the getCube of the number.
     */
    private int getCube(int numberForCubing){
        return numberForCubing * numberForCubing * numberForCubing;            // i don't want to use class Math :)
    }
}