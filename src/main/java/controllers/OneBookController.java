package controllers;

import controllers.constants.Attributes;
import entity.Book;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Get info about one book from storage.
 */
public class OneBookController extends AbstractBookController{

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String bookTitle = request.getParameter(Attributes.BOOK_TITLE);
        Book book = bookDao.getBookByTitle(bookTitle);      // get book from storage
        request.setAttribute(Attributes.BOOK, book);
        request.getRequestDispatcher("jsp/book.jsp").forward(request, response);
    }
}