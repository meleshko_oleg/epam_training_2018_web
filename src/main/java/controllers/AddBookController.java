package controllers;

import controllers.constants.Attributes;
import entity.Author;
import entity.Book;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Add a new book to storage.
 */
public class AddBookController extends AbstractBookController{

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int bookId = Integer.parseInt(request.getParameter(Attributes.BOOK_ID));
        String bookTitle = request.getParameter(Attributes.BOOK_TITLE);
        String bookDescription = request.getParameter(Attributes.BOOK_DESCRIPTION);
        int bookPrice = Integer.parseInt(request.getParameter(Attributes.BOOK_PRICE));
        String authorName = request.getParameter(Attributes.BOOK_AUTHOR);
        int authorId = Integer.parseInt(request.getParameter(Attributes.AUTHOR_ID));
        Author bookAuthor = new Author(authorId, authorName);
        Book newBook = new Book(bookId, bookTitle, bookAuthor,bookDescription, bookPrice);
        bookDao.saveBook(newBook);          // add book to storage
        response.sendRedirect("/books");
    }
}