package controllers;

import controllers.constants.Attributes;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Update existed book in storage.
 */
public class UpdateBookPriceController extends AbstractBookController{

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String bookTitle = request.getParameter(Attributes.BOOK_TITLE);
        int newBookPrice = Integer.parseInt(request.getParameter(Attributes.NEW_BOOK_PRICE));
        bookDao.updateBookPrice(bookTitle, newBookPrice);       // update book price in storage
        response.sendRedirect("/books");
    }
}