package controllers.tagControllers;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Get url-pattern for simple tag.
 */
public class SimpleTagController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final String JSP_URL_PARAMETER = "url";
        final String JSP_URL_TO_TEST_ATTRIBUTE = "url_to_test";

        String string = request.getParameter(JSP_URL_PARAMETER);
        request.setAttribute(JSP_URL_TO_TEST_ATTRIBUTE, string);
        request.getRequestDispatcher("/jsp/tags.jsp").forward(request, response);
    }
}