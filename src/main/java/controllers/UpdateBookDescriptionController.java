package controllers;

import controllers.constants.Attributes;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Update book description in storage.
 */
public class UpdateBookDescriptionController  extends AbstractBookController{

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String bookTitle = request.getParameter(Attributes.BOOK_TITLE);
        String newBookDescription = request.getParameter(Attributes.BOOK_DESCRIPTION);
        bookDao.updateBookDescription(bookTitle, newBookDescription);       // update book description in storage
        response.sendRedirect("/books");
    }
}