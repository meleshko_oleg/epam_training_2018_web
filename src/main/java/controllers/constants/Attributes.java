package controllers.constants;

/**
 * Attributes.
 */
public final class Attributes {
    public static final String BOOK_ID = "book_id";
    public static final String BOOK_TITLE = "book_title";
    public static final String BOOK_DESCRIPTION = "book_description";
    public static final String BOOK_PRICE = "book_price";
    public static final String NEW_BOOK_PRICE = "new_book_price";
    public static final String BOOK_AUTHOR = "author_name";
    public static final String NEW_BOOK_AUTHOR = "new_book_author";
    public static final String AUTHOR_ID = "author_id";
    public static final String BOOKS = "books";
    public static final String BOOK = "book";
    public static final String BOOK_DAO = "DAO";
}