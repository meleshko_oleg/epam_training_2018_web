package customTags;

import javax.servlet.ServletRegistration;
import javax.servlet.jsp.tagext.TagSupport;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Classic tag example.
 */
public class ClassicTag extends TagSupport {
    private AtomicInteger numberOfUrls;
    private Map<String, ? extends ServletRegistration> servletRegistrations;
    private Object[] servletNames;

    @Override
    public int doStartTag() {
        init();
        setJspAttributes();
        return EVAL_BODY_INCLUDE;
    }

    @Override
    public int doAfterBody() {
        while (numberOfUrls.intValue() >= 0) {
            setJspAttributes();
            return EVAL_BODY_AGAIN;
        }
        return SKIP_BODY;
    }

    /**
     * Initialization of main parameters.
     */
    private void init() {
        servletRegistrations = pageContext.getRequest().getServletContext().getServletRegistrations();
        numberOfUrls = new AtomicInteger(servletRegistrations.size() - 1);      // for working with an array
        servletNames = servletRegistrations.keySet().toArray();
    }

    /**
     * Set attributes for the jsp page.
     */
    private void setJspAttributes() {
        final String JSP_URL_ATTRIBUTE = "url";
        final String JSP_SERVLET_ATTRIBUTE = "servlet";
        final String SERVLET_NAME = String.valueOf(servletNames[numberOfUrls.intValue()]);

        pageContext.setAttribute(JSP_URL_ATTRIBUTE, servletRegistrations.get(SERVLET_NAME).getMappings());
        pageContext.setAttribute(JSP_SERVLET_ATTRIBUTE, SERVLET_NAME);

        numberOfUrls.decrementAndGet();
    }
}