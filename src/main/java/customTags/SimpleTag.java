package customTags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

/**
 * Simple tag example.
 */
public class SimpleTag extends SimpleTagSupport {
    private String url;     // tag attribute

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public void doTag() {
        final String JSP_URL_ATTRIBUTE = "url";
        final String JSP_RESOURCE_ATTRIBUTE = "resource";

        /* find web component by url and send info to jsp :) */
        ((PageContext) getJspContext())
                .getServletContext()
                    .getServletRegistrations()
                        .forEach((servletName, servletRegistration) -> servletRegistration.getMappings().stream()
                            .filter(s -> s.equals(url))
                            .forEach(s -> {
                                            getJspContext().setAttribute(JSP_URL_ATTRIBUTE, url);
                                            getJspContext().setAttribute(JSP_RESOURCE_ATTRIBUTE, servletName);
                                            try {
                                                getJspBody().invoke(null);
                                            }
                                            catch (JspException | IOException e) {
                                                e.printStackTrace();
                                            }
                            }));
    }
}