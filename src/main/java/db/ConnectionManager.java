package db;

import org.apache.log4j.Logger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * Connection manager.
 */
public class ConnectionManager {
    private static final Logger LOGGER = Logger.getLogger(ConnectionManager.class);
    private static final ResourceBundle MYSQL_INFO = ResourceBundle.getBundle(ResourceBundleKeys.MYSQL);
    private static final String DRIVER_NAME = MYSQL_INFO.getString(ResourceBundleKeys.DRIVER);
    private static final String DB_URL = MYSQL_INFO.getString(ResourceBundleKeys.URL);
    private static final String USER_NAME = MYSQL_INFO.getString(ResourceBundleKeys.USER_NAME);
    private static final String PASSWORD = MYSQL_INFO.getString(ResourceBundleKeys.USER_PASSWORD);
    private static ConnectionManager ourInstance = new ConnectionManager();
    private Connection connection;

    private ConnectionManager() {
    }

    /**
     * Work with singleton.
     * @return a single instance of ConnectionManager.
     */
    public static ConnectionManager getInstance() {
        return ourInstance;
    }

    /**
     * Open connection to database.
     */
    public void openConnection(){
        try {
            Class.forName(DRIVER_NAME);
        }
        catch (ClassNotFoundException e) {
            LOGGER.error(e.getMessage(), e);
        }
        if (connection == null) {
            try {
                connection = DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);
            }
            catch (SQLException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }

    /**
     * Close connection to database.
     */
    public void closeConnection(){
        try {
            if (connection != null) {
                connection.close();
                connection = null;
            }
        }
        catch (SQLException e){
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * Get PreparedStatement for database from connection.
     * @param sql SQL query.
     * @return an instance of PreparedStatement.
     */
    public PreparedStatement getPreparedStatement(String sql){
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement =  connection.prepareStatement(sql);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return preparedStatement;
    }

    /**
     * Set auto commit.
     * @param autoCommitFlag True or false.
     */
    public void setAutoCommit(boolean autoCommitFlag){
        try {
            connection.setAutoCommit(autoCommitFlag);
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * Commit info to database.
     */
    public void commit(){
        try {
            connection.commit();
        }
        catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
            try {
                connection.rollback();
            } catch (SQLException e1) {
                LOGGER.error(e.getMessage(), e1);
            }
        }
    }

    /**
     * Keys for MySQL.properties file.
     */
    private class ResourceBundleKeys{
        private final static String MYSQL = "MySQL";
        private final static String DRIVER = "driver";
        private final static String URL = "url";
        private final static String USER_NAME = "username";
        private final static String USER_PASSWORD = "userpassword";
    }
}