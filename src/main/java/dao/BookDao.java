package dao;

import entity.Book;
import java.util.List;

/**
 * Main interface.
 */
public interface BookDao {

    /**
     * Get all books from storage.
     * @return a list with books.
     */
    List<Book> getAllBooks();

    /**
     * Save book to storage.
     * @param book New book.
     */
    void saveBook(Book book);

    /**
     * Delete book from storage.
     * @param bookTitle Title of the book.
     */
    void deleteBook(String bookTitle);

    /**
     * Update book price using book title.
     * @param bookTitle Title of the book.
     * @param newBookPrice New price of the book.
     */
    void updateBookPrice(String bookTitle, int newBookPrice);

    /**
     * Get book from storage using book title.
     * @param bookTitle Title of the book.
     * @return an instance of the book.
     */
    Book getBookByTitle(String bookTitle);

    /**
     * Update book description using book title.
     * @param bookTitle Title of the book in the storage.
     * @param newBookDescription New description for the book.
     */
    void updateBookDescription(String bookTitle, String newBookDescription);

    /**
     * Update book author using book id.
     * @param newBookAuthorName New author of the book.
     * @param bookId Book id.
     */
    void updateBookAuthor(String newBookAuthorName, int bookId);

    /**
     * Final action with the storage.
     */
    void finishWork();
}