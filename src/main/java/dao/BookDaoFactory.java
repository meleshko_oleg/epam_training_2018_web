package dao;

import dao.implementations.BookDaoImplDb;

/**
 * Return Book dao (DB, RAM or FILE).
 */
public class BookDaoFactory {
    private static BookDao BOOK_DAO;

    /**
     * Factory method for BookDao implementation.
     * @param storage The variant of storage.
     * @return an instance of BookDao implementation.
     */
    public static BookDao getBookDao(String storage){
        checkBookDao(storage);
        return BOOK_DAO;
    }

    /**
     * Check if BookDao implementation already exists.
     * @param storage The variant of storage.
     */
    private static void checkBookDao(String storage){
        if (BOOK_DAO == null){
            BOOK_DAO = Storage.valueOf(storage).getDao();
        }
    }

    /**
     * Some variants of storage
     */
    private enum Storage{
        DB {
            @Override
            BookDao getDao() {
                return new BookDaoImplDb();
            }
        }/*,
        RAM {
            @Override
            BookDao getDao() {
                return new BookDaoImplRAM();
            }
        },
        FILE {
            @Override
            BookDao getDao() {
                return new BookDaoImplFile();
            }
        }*/;

        abstract BookDao getDao();
    }
}