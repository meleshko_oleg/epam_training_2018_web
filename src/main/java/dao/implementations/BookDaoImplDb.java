package dao.implementations;

import dao.BookDao;
import db.ConnectionManager;
import entity.Author;
import entity.Book;
import org.apache.log4j.Logger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Main implementation of BookDao interface.
 */
public class BookDaoImplDb implements BookDao {
    private static final Logger LOGGER = Logger.getLogger(BookDaoImplDb.class);
    private ConnectionManager connectionManager;

    public BookDaoImplDb() {
        connectionManager = ConnectionManager.getInstance();
        connectionManager.openConnection();
    }

    /**
     * Save book to database.
     * @param book New book.
     */
    @Override
    public void saveBook(Book book) {
        final int BOOK_ID_INDEX = 1;
        final int BOOK_TITLE_INDEX = 2;
        final int BOOK_DESCRIPTION_INDEX = 3;
        final int BOOK_PRICE_INDEX = 4;
        final int AUTHOR_ID_INDEX = 1;
        final int BOOK_ID = 2;
        final int AUTHOR_NAME = 3;

        try {
            connectionManager.setAutoCommit(false);

            // save info about book
            try (PreparedStatement preparedStatement = connectionManager.getPreparedStatement(SQLQueries.SAVE_BOOK)) {
                preparedStatement.setInt(BOOK_ID_INDEX, book.getId());
                preparedStatement.setString(BOOK_TITLE_INDEX, book.getTitle());
                preparedStatement.setString(BOOK_DESCRIPTION_INDEX, book.getDescription());
                preparedStatement.setInt(BOOK_PRICE_INDEX, book.getPrice());
                preparedStatement.executeUpdate();
            }
            catch (SQLException e) {
                LOGGER.error(e.getMessage(), e);
            }

            // save info about book author
            try (PreparedStatement preparedStatement = connectionManager.getPreparedStatement(SQLQueries.SAVE_BOOK_AUTHOR)) {
                preparedStatement.setInt(AUTHOR_ID_INDEX, book.getAuthor().getId());
                preparedStatement.setInt(BOOK_ID, book.getId());
                preparedStatement.setString(AUTHOR_NAME, book.getAuthor().getName());
                preparedStatement.executeUpdate();
            }
            catch (SQLException e) {
                LOGGER.error(e.getMessage(), e);
            }
            connectionManager.commit();
        }
        finally {
            connectionManager.setAutoCommit(true);
        }
    }

    /**
     * Delete book from database.
     * @param bookTitle Title of the book.
     */
    @Override
    public void deleteBook(String bookTitle) {
        final int BOOK_TITLE_INDEX = 1;

        try(PreparedStatement preparedStatement = connectionManager.getPreparedStatement(SQLQueries.DELETE_BOOK)){
            preparedStatement.setString(BOOK_TITLE_INDEX, bookTitle);
            preparedStatement.executeUpdate();
        }
        catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * Update book price in database.
     * @param bookTitle Title of the book.
     * @param newBookPrice New price of the book.
     */
    @Override
    public void updateBookPrice(String bookTitle, int newBookPrice) {
        final int NEW_BOOK_PRICE_INDEX = 1;
        final int BOOK_TITLE_INDEX = 2;

        try(PreparedStatement preparedStatement = connectionManager.getPreparedStatement(SQLQueries.UPDATE_BOOK_PRICE)){
            preparedStatement.setInt(NEW_BOOK_PRICE_INDEX, newBookPrice);
            preparedStatement.setString(BOOK_TITLE_INDEX, bookTitle);
            preparedStatement.executeUpdate();
        }
        catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * Get all books from database.
     * @return a list of books.
     */
    @Override
    public List<Book> getAllBooks(){
        final int BOOK_ID_INDEX = 1;
        final int BOOK_TITLE_INDEX = 2;
        final int BOOK_DESCRIPTION_INDEX = 3;
        final int BOOK_PRICE_INDEX = 4;

        List<Book> bookList = new ArrayList<>();
        try(PreparedStatement preparedStatement = connectionManager.getPreparedStatement(SQLQueries.GET_ALL_BOOKS);
            ResultSet resultSet = preparedStatement.executeQuery()){
            while (resultSet.next()){
                int bookId = resultSet.getInt(BOOK_ID_INDEX);
                String bookTitle = resultSet.getString(BOOK_TITLE_INDEX);
                String bookDescription = resultSet.getString(BOOK_DESCRIPTION_INDEX);
                int bookPrice = resultSet.getInt(BOOK_PRICE_INDEX);
                Author bookAuthor = getAuthorByBookId(bookId);  // using additional method
                Book book = new Book(bookId, bookTitle, bookAuthor, bookDescription, bookPrice);
                bookList.add(book);
            }
        }
        catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return bookList;
    }

    /**
     * Additional method for getting book author using book id.
     * @param bookId Id of the book.
     * @return an author of the book.
     */
    private Author getAuthorByBookId(int bookId){
        final int BOOK_ID_INDEX = 1;
        final int AUTHOR_ID_INDEX = 1;
        final int AUTHOR_NAME_INDEX = 2;

        int authorId = 0;
        String authorName = "";

        try (PreparedStatement preparedStatement = connectionManager.getPreparedStatement(SQLQueries.GET_BOOK_AUTHOR)){
            preparedStatement.setInt(BOOK_ID_INDEX, bookId);
            try(ResultSet resultSet = preparedStatement.executeQuery()){
                while (resultSet.next()) {
                    authorId = resultSet.getInt(AUTHOR_ID_INDEX);
                    authorName = resultSet.getString(AUTHOR_NAME_INDEX);
                }
            }
        }
        catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return new Author(authorId, authorName);
    }

    /**
     * Get book from database using book title.
     * @param bookTitle Title of the book.
     * @return a book from database.
     */
    @Override
    public Book getBookByTitle(String bookTitle) {
        final int BOOK_TITLE_INDEX = 1;
        final int BOOK_ID_INDEX = 1;
        final int BOOK_DESCRIPTION_INDEX = 2;
        final int BOOK_PRICE_INDEX = 3;
        final int AUTHOR_ID_INDEX = 1;
        final int AUTHOR_NAME_INDEX = 2;

        int bookId = 0;
        String bookDescription = "";
        int bookPrice = 0;
        int authorId = 0;
        String authorName = "";

        // get book info
        try {
            try(PreparedStatement preparedStatement = connectionManager.getPreparedStatement(SQLQueries.GET_BOOK_BY_TITLE)){
                preparedStatement.setString(BOOK_TITLE_INDEX, bookTitle);
                try(ResultSet resultSet = preparedStatement.executeQuery()){
                    while (resultSet.next()){
                        bookId = resultSet.getInt(BOOK_ID_INDEX);
                        bookDescription = resultSet.getString(BOOK_DESCRIPTION_INDEX);
                        bookPrice = resultSet.getInt(BOOK_PRICE_INDEX);
                    }
                }
            }
        }
        catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
        }

        // get author info
        try {
            try(PreparedStatement preparedStatement = connectionManager.getPreparedStatement(SQLQueries.GET_BOOK_AUTHOR)){
                preparedStatement.setInt(BOOK_ID_INDEX, bookId);
                try(ResultSet resultSet = preparedStatement.executeQuery()){
                    while (resultSet.next()){
                        authorId = resultSet.getInt(AUTHOR_ID_INDEX);
                        authorName = resultSet.getString(AUTHOR_NAME_INDEX);
                    }
                }
            }
        }
        catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
        }

        Author author = new Author(authorId, authorName);
        return new Book(bookId, bookTitle, author, bookDescription, bookPrice);
    }

    /**
     * Update book description in database.
     * @param bookTitle Title of the book in the storage.
     * @param newBookDescription New description for the book.
     */
    @Override
    public void updateBookDescription(String bookTitle, String newBookDescription) {
        final int NEW_BOOK_DESCRIPTION_INDEX = 1;
        final int BOOK_TITLE_INDEX = 2;

        try(PreparedStatement preparedStatement = connectionManager.getPreparedStatement(SQLQueries.UPDATE_BOOK_DESCRIPTION)){
            preparedStatement.setString(NEW_BOOK_DESCRIPTION_INDEX, newBookDescription);
            preparedStatement.setString(BOOK_TITLE_INDEX, bookTitle);
            preparedStatement.executeUpdate();
        }
        catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * Update book author in database.
     * @param newBookAuthorName New author of the book.
     * @param bookId Book id.
     */
    @Override
    public void updateBookAuthor(String newBookAuthorName, int bookId) {
        final int NEW_BOOK_AUTHOR_NAME = 1;
        final int BOOK_ID_INDEX = 2;

        try(PreparedStatement preparedStatement = connectionManager.getPreparedStatement(SQLQueries.UPDATE_BOOK_AUTHOR)){
            preparedStatement.setString(NEW_BOOK_AUTHOR_NAME, newBookAuthorName);
            preparedStatement.setInt(BOOK_ID_INDEX, bookId);
            preparedStatement.executeUpdate();
        }
        catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * Final action with database.
     */
    @Override
    public void finishWork(){
        connectionManager.closeConnection();
    }

    /**
     * Queries to MySQL database
     */
    private class SQLQueries{
        private static final String GET_ALL_BOOKS = "SELECT book_id, book_title, book_description, book_price FROM books.my_books";
        private static final String SAVE_BOOK = "INSERT INTO books.my_books (book_id, book_title, book_description, book_price) VALUES (?, ?, ?, ?)";
        private static final String SAVE_BOOK_AUTHOR = "INSERT INTO books.my_authors (author_id, book_id, author_name) VALUES (?, ?, ?)";
        private static final String DELETE_BOOK = "DELETE FROM books.my_books WHERE book_title = ?";
        private static final String UPDATE_BOOK_PRICE = "UPDATE books.my_books SET book_price = ? WHERE book_title = ?";
        private static final String GET_BOOK_AUTHOR = "SELECT author_id, author_name FROM books.my_authors WHERE book_id = ?";
        private static final String GET_BOOK_BY_TITLE = "SELECT book_id, book_description, book_price FROM books.my_books WHERE book_title = ?";
        private static final String UPDATE_BOOK_DESCRIPTION = "UPDATE books.my_books SET book_description = ? WHERE book_title = ?";
        private static final String UPDATE_BOOK_AUTHOR = "UPDATE books.my_authors SET author_name = ? WHERE book_id = ?";
    }
}