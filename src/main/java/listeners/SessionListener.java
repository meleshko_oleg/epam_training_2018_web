package listeners;

import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Session activation/deactivation listener.
 */
public class SessionListener implements HttpSessionListener{
    private static final Logger LOGGER = Logger.getLogger(SessionListener.class);
    private static final String ACTIVATION_MESSAGE = "Session with session id = %s was activated";
    private static final String DEACTIVATION_MESSAGE = "Session with session id = %s was destroyed. Duration = %d seconds";

    /**
     * Logging info if session is created.
     * @param sessionEvent New session.
     */
    @Override
    public void sessionCreated(HttpSessionEvent sessionEvent) {
        String sessionID = sessionEvent.getSession().getId();
        LOGGER.info(String.format(ACTIVATION_MESSAGE, sessionID));
    }

    /**
     * Logging info if session is destroyed.
     * @param sessionEvent Existing session.
     */
    @Override
    public void sessionDestroyed(HttpSessionEvent sessionEvent) {
        final int SECOND_MILLISECOND_RATIO = 1000;
        HttpSession httpSession = sessionEvent.getSession();
        String sessionID = httpSession.getId();
        long sessionCreationTime = httpSession.getCreationTime();
        long currentTime = System.currentTimeMillis();
        long sessionDurationInSeconds = (currentTime - sessionCreationTime) / SECOND_MILLISECOND_RATIO;
        LOGGER.info(String.format(DEACTIVATION_MESSAGE, sessionID, sessionDurationInSeconds));
    }
}