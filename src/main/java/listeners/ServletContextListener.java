package listeners;

import controllers.constants.Attributes;
import dao.BookDao;
import dao.BookDaoFactory;
import org.apache.log4j.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletRegistration;
import java.util.Map;

/**
 * Get app information.
 */
public class ServletContextListener implements javax.servlet.ServletContextListener {
    private static final Logger LOGGER = Logger.getLogger(ServletContextListener.class);

    /**
     * Initializing of ServletContext and logging main information about application.
     * @param servletContextEvent Event for servlet context.
     */
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        logAppInfo(servletContextEvent);
    }

    /**
     * Logging all information of the application.
     * @param servletContextEvent Event for servlet context.
     */
    private void logAppInfo(ServletContextEvent servletContextEvent){
        ServletContext servletContext = servletContextEvent.getServletContext();
        String servletContextName = "Servlet context name: " + servletContext.getServletContextName() + "\n";
        String serverInfo = "Server info: " + servletContext.getServerInfo() + "\n";
        String virtualServerName = "Virtual server name: " + servletContext.getVirtualServerName() + "\n";
        String majorVersion = "Major version: " + servletContext.getMajorVersion() + "\n";
        String minorVersion = "Minor version: " + servletContext.getMinorVersion() + "\n";
        String allServletsInfo = getAllServletsInfo(servletContextEvent);
        LOGGER.info(servletContextName + serverInfo + virtualServerName + majorVersion + minorVersion + allServletsInfo);
    }

    /**
     * Decoration for servlet information.
     * @param servletContextEvent Event for servlet context.
     * @return string with servlet info after a little decoration.
     */
    private String getAllServletsInfo(ServletContextEvent servletContextEvent){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("All servlets:\n");
        Map<String, ? extends ServletRegistration> servletRegistrations = servletContextEvent.getServletContext().getServletRegistrations();
        for(Map.Entry<String, ? extends ServletRegistration> entry: servletRegistrations.entrySet()){
            stringBuilder.append("\t\t\t\t" + entry.getKey() + ": " + entry.getValue().getClassName() + "\n");
        }
        return stringBuilder.toString();
    }

    /**
     * Final action.
     * @param servletContextEvent Event for servlet context.
     */
    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        ServletContext servletContext = servletContextEvent.getServletContext();
        String storage = servletContext.getInitParameter(Attributes.BOOK_DAO);
        BookDao bookDao = BookDaoFactory.getBookDao(storage);
        bookDao.finishWork();
    }
}