package listeners;

import org.apache.log4j.Logger;
import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;
import java.io.IOException;

/**
 * Asynchronous context listener.
 */
public class AsyncContextListener implements AsyncListener {
    private static final Logger LOGGER = Logger.getLogger(AsyncContextListener.class);
    private static final String ON_COMPLETE_MESSAGE = "Asynchronous operation has finished";

    /**
     * Log info when asyncContext.complete() is invoked.
     * @param asyncEvent asynchronous event.
     */
    @Override
    public void onComplete(AsyncEvent asyncEvent) throws IOException {
        LOGGER.info(ON_COMPLETE_MESSAGE);
    }

    @Override
    public void onStartAsync(AsyncEvent asyncEvent) throws IOException {
        /* NOP */
    }

    @Override
    public void onTimeout(AsyncEvent asyncEvent) throws IOException {
        /* NOP */
    }

    @Override
    public void onError(AsyncEvent asyncEvent) throws IOException {
        /* NOP */
    }
}