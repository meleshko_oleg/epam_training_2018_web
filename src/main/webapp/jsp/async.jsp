<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Async test</title>
    </head>
    <body style="text-align: center">
        <h3>
            AsyncServlet testing
        </h3>
        <form action="/asynchronous" method="POST">
            Find the square of a number with a simple servlet:<br>
            <input type="text" name="square" placeholder="for simple servlet"><br>
            Find the cube of a number with asynchronous servlet:<br>
            <input type="text" name="cube" placeholder="for async servlet"><br>
            <input type="submit" value="Calculate">
        </form>
    </body>
</html>
<%-------------------------------- work asynchronously --------------------------------%>