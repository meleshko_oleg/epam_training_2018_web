<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>ST</title>
</head>
<body>
    <h3>Enter url: </h3>
    <form action="<c:url value="/simple"/>" method="post">
        <input type="text" name="url" placeholder="for simple tag">
        <input type="submit">
    </form>
</body>
</html>
<%------------ use simple tag to resolve web component for the posted url ------------%>