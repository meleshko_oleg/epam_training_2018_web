<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Delete book</title>
    </head>
    <body>
        <h1>
            Delete book
        </h1>
        <form action="<c:url value="/bookDelete"/>" method="POST">
            <input type="text" name="book_title" placeholder="enter book title">
            <input type="submit" value="Delete">
        </form>
    </body>
</html>
<%--------------------------- delete existed book ---------------------------%>