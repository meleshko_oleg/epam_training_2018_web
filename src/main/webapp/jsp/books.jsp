<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Books</title>
    </head>
    <body>
        <h1>
            Books
        </h1>
        <div>
            <c:forEach var="book" items="${books}" varStatus="Count">
                <p>
                    <nobr>
                    <form id="${Count.count}" action="<c:url value="/book"/>" method="GET">
                        <a href="#" onclick="document.getElementById('${Count.count}').submit(); return false;">${book.title}</a>
                        <input type="hidden" name="book_title" value="${book.title}">
                    </form>

                    by ${book.author}, ${book.price}$ ---> Description: ${book.description}
                   </nobr>

                </p>
            </c:forEach>
        </div>
        <a href="<c:url value="/jsp/addBook.jsp"/>">Add new book</a><br>
        <a href="<c:url value="/jsp/deleteBook.jsp"/>">Delete existed book</a><br>
        <a href="<c:url value="/jsp/async.jsp"/>" style="position: fixed; top: 5%; right: 5%">Asynchronous operation</a>
        <a href="<c:url value="/jsp/findUrl.jsp"/>" style="position: fixed; top: 10%; right: 5%">Tags</a>
    </body>
</html>
<%--------------------------- all books ---------------------------%>