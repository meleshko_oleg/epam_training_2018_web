<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Add book</title>
    </head>
    <body>
        <h1>
            New book
        </h1>
        <form action="<c:url value="/bookAdd"/>" method="POST">
            <input type="text" name="book_id" placeholder="enter book id">
            <input type="text" name="book_title" placeholder="enter book title">
            <input type="text" name="book_description" placeholder="enter book description">
            <input type="text" name="book_price" placeholder="enter book price">
            <input type="text" name="author_id" placeholder="enter author id">
            <input type="text" name="author_name" placeholder="enter author_name">
            <input type="submit" value="Add">
        </form>
    </body>
</html>
<%--------------------------- add new book ---------------------------%>