<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Async result</title>
    </head>
    <body>
        Square: ${square}<br>
        Cube: ${cube}<br>
        <a href="<c:url value="/books"/>">Main page</a>
    </body>
</html>
<%------------------------ Result of asynchronous servlet --------------------------------%>