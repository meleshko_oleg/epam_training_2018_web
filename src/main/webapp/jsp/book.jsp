<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Book</title>
    </head>
    <body>
        Book title: ${book.title}<br>
        Book author: ${book.author}<br>
        Book price: ${book.price}$<br>
        Book description: ${book.description}<br>
        <h3>
            Update book description
        </h3>
        <form action="<c:url value="/bookUpdateDescription"/>" method="POST">
            <input type="text" name="book_description" placeholder="new description">
            <input type="hidden" name="book_title" value="${book.title}">
            <input type="submit" value="Update description">
        </form>
        <h3>
            Update book author
        </h3>
        <form action="<c:url value="/bookUpdateAuthor"/>" method="POST">
            <input type="text" name="new_book_author" placeholder="enter new book author">
            <input type="hidden" name="book_id" value="${book.id}">
            <input type="submit" value="Update author">
        </form>
        <h3>
            Update book price
        </h3>
        <form action="<c:url value="/bookUpdatePrice"/>" method="POST">
            <input type="text" name="new_book_price" placeholder="enter new book price">
            <input type="hidden" name="book_title" value="${book.title}">
            <input type="submit" value="Update price">
        </form>
        <h3>
            Add book cover
        </h3>
        <form action="<c:url value="/uploadBookCover"/>" method="POST" enctype="multipart/form-data">
            <input type="file" name="file" id="file"/>
            <input type="hidden" name="book_title" value="${book.title}">
            <input type="submit" value="Upload cover"/>
        </form>
    </body>
</html>
<%--------------------------- info about one book ---------------------------%>