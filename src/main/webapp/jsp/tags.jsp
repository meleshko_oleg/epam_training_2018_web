<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="utils" uri="http://utils.tag" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Tags</title>
    </head>
    <body>
        <%-- Classic tag --%>
        <utils:listmapping>
            ${url} - ${servlet}<br>
        </utils:listmapping>

        ---------------------------------<br>

        <%-- Simple tag --%>
        <utils:resolveurl url="${url_to_test}">
            ${url} - ${resource}
        </utils:resolveurl>
    </body>
</html>